(ns plf02.core)

(defn función-associative?-1 [x] (associative? x))
(función-associative?-1 [1 2 3])
(función-associative?-1 (fn [a b c]{:a 1 :b 2 :c 3} ))
(función-associative?-1 '(1 2 3))

(defn función-boolean?-1 [x] (boolean? x ))
(función-boolean?-1 false)
(función-boolean?-1 0)
(función-boolean?-1 (boolean 3) )


(defn función-char?-1 [x] (char? x) )
(función-char?-1 \a)
(función-char?-1 2)
(función-char?-1 "Eromanga Sensei ")



(defn función-coll?-1[x] (coll? x))
(defn función-coll?-2 [x] (coll? x))
(defn función-coll?-3[x y] (coll? (+ x y)))
(función-coll?-1 [1 2 3])
(función-coll?-2 '("c1" "c2"))
(función-coll?-3 3 4)




(defn función-decimal?-1 [x] (decimal? x))
(defn función-decimal?-2 [x] (decimal? x))
(defn función-decimal?-3 [x y](decimal? (+ x y)))
(función-decimal?-1 [1 2 3 4])
(función-decimal?-2 "Misaka Mikoto")
(función-decimal?-3 60M 3)





(defn función-double?-1[x] (double? x))
(defn función-double?-2 [x](double? (last x)))
(defn función-double?-3[x y](double? (/ x y)))
(función-double?-1 9.0)
(función-double?-2 [0 2.3 4.3])
(función-double?-3 3.0 2)



(defn función-float?-1[x](float? x))
(defn función-float?-2[x y] (float? (/ x y)))
(defn función-float?-3[x y z] (float? (/ x y z)))
(función-float?-1 1.0)
(función-float?-2 3 2.1)
(función-float?-3 4 5 4)



(defn función-ident?-1 [x] (ident? x))
(defn función-ident?-2 [x](ident? x))
(defn función-ident?-3 [x y](ident? (conj x y)))
(función-ident?-1 '123454321)
(función-ident?-1 :a)
(función-ident?-3 [1 2 3] :a)


(defn función-indexed?-1[x](indexed? x))
(defn función-indexed?-2[x](indexed? x))
(defn función-indexed?-3[x y](indexed? (x y)))
(función-indexed?-1 [1 2 3])
(función-indexed?-2 [1 2 3 {:a 1 :b 2 :c 3}])
(función-indexed?-3 [:a "Susumiya" :b "Haruhi" :c "No Yutsu"] 1)




(defn función-int?-1 [x] (int? x))
(defn función-int?-2 [x y](int? (+ x y)))
(defn función-int?-3[x y z](int? (+ x y z)))

(función-int?-1 1)
(función-int?-2 1 2)
(función-int?-3 1 2 3.0)


(defn función-integer?-1 [x](integer? x))
(defn función-integer?-2 [x y](integer? (+ x y)))
(defn función-integer?-3 [x y z](integer? (+ x y z)))

(función-integer?-1 70)
(función-integer?-2 1 2.2)
(función-integer?-3 1 2 3)


(defn función-keyword?-1 [x](keyword? x))
(defn función-keyword?-2 [x](keyword? x))
(defn función-keyword?-3 [x](keyword? x))
(función-keyword?-1 :a)
(función-keyword?-2 {:a 1 :b 2 :c 10})
(función-keyword?-3 :b)


(defn función-list?-1 [x] (list? x))
(defn función-list?-2 [x] (list? x))
(defn función-list?-3 [x] (list? x))
(función-list?-1 '(1 2 3))
(función-list?-2 {:a 1 :b 2 :c 3})
(función-list?-3 [1 2 3])



(defn función-map-entry?-1[x](map-entry? x))
(defn función-map-entry?-2[x](map-entry? x))
(defn función-map-entry?-3[x](map-entry? x))
(función-map-entry?-1 #{1 2 3})
(función-map-entry?-2 {:a "Hatsune " :b "Miku" })
(función-map-entry?-3 {:a \a})



(defn función-map?-1 [x](map? x))
(defn función-map?-2[x](map? x))
(defn función-map?-3[x] (map? x))

(función-map?-1 {:a "Akiyama" :b "Mio"})
(función-map?-2 \c)
(función-map?-3 '(1 2 3 ))


(defn función-nat-int?-1[x](nat-int? x))
(defn función-nat-int?-2[x y](nat-int? (+ x y)))
(defn función-nat-int?-3[x y z](nat-int? (+ x y z)))
(función-nat-int?-1 \a)
(función-nat-int?-2 21 9.0)
(función-nat-int?-3 21 9 43)


(defn función-number?-1[x](number? x))
(defn función-number?-2[x](number? x))
(defn función-number?-3[x](number? x))

(función-number?-1 1)
(función-number?-2 2.3)
(función-number?-3 "Index")


(defn función-pos-int?-1[x](pos-int? x))
(defn función-pos-int?-2[x](pos-int? x))
(defn función-pos-int?-3[x](pos-int? x))
(función-pos-int?-1 21)
(función-pos-int?-2 -1)
(función-pos-int?-3 ( *(- 1) 1 2))


(defn función-ratio?-1[x](ratio? x))
(defn función-ratio?-2[x](ratio? x))
(defn función-ratio?-3[x] (ratio? x))
(función-ratio?-1 2/1)
(función-ratio?-2 2/3)
(función-ratio?-3 \a)




(defn función-rational?-1[x](rational? x))
(defn función-rational?-2 [x](rational? x))
(defn función-rational?-3[x](rational? x))

(función-rational?-1 21)
(función-rational?-2 21/9)
(función-rational?-3 (- 0.1 0.2))


(defn función-seq?-1 [x](seq? x))
(defn función-seq?-2 [x] (seq? x))
(defn función-seq?-3[x] (seq? x))
(función-seq?-1 '( 1 2 3))
(función-seq?-2 '(2 4 6 8))
(función-seq?-3 [3 1 4 5])


(defn función-seqable?-1 [x] (seqable? x))
(defn función-seqable?-2[x](seqable? x))
(defn función-seqable?-3 [x] (seqable? x))
(función-seqable?-1 [1 2 3 5 8 13])
(función-seqable?-2 '(1 2 3 4))
(función-seqable?-3 ["1" "2" "3" "5"])



(defn función-sequential?-1[x] (sequential? x))
(defn función-sequential?-2[x] (sequential? x))
(defn función-sequential?-3[x](sequential? x))
(función-sequential?-1 [1 2 3 5 8 13])
(función-sequential?-2 '(1 2 3 5 8 13))
(función-sequential?-3 #{1 2 3 5 8 13})



(defn función-set?-1[x](set? x))
(defn función-set?-2 [x](set? x))
(defn función-set?-3[x](set? x))
(función-set?-1 #{1 2 3 5 8 13})
(función-set?-2 '(1 2 3 5 8 13))
(función-set?-3 [1 2 3 5 8 13])




(defn función-some?-1 [x](some? x))
(defn función-some?-2[x](some? x))
(defn función-some?-3[x](some? x))
(función-some?-1 21)
(función-some?-2 "Asuna")
(función-some?-3 \a)

(defn función-string?-1 [x] (string? x))
(defn función-string?-2 [x](string? x))
(defn función-string?-3[x](string? x))

(función-string?-1 "Yuko")
(función-string?-2 ["C.C" "Misaki" "Mikuru" 5])
(función-string?-3 ["Manami" "Kirino" "Ayase"] )




(defn función-symbol?-1[x](symbol? x))
(defn función-symbol?-2[x](symbol? x))
(defn función-symbol?-3[x](symbol? x))
(función-symbol?-1 "Shana")
(función-symbol?-2 'OnegaiTeacher)
(función-symbol?-3 1)



(defn función-vector?-1[x](vector? x))
(defn función-vector?-2[x](vector? x))
(defn función-vector?-3[x](vector? x))

(función-vector?-1 ["Onichichi" "Bible Black"])
(función-vector?-2  '("Fukubuki"))
(función-vector?-3 #{:a 1 :b 2 :c 3} )


; Parte 2 Funciones de ordenb superior

(defn funcion-drop-1[x y] (drop x y))
(defn funcion-drop-2[x y] (drop x y))
(defn funcion-drop-3[x y] (drop x y))
(funcion-drop-1 5 [1 2 3 5 8 13 21 34])
(funcion-drop-2 1 '(1 2 3 5 8 13 21 34))
(funcion-drop-3 3 #{1 2 3 5 8 13 21 34})


(defn funcion-drop-last-1[x] (drop-last x))
(defn funcion-drop-last-2[x y] (drop-last x y))
(defn funcion-drop-last-3[x y] (drop-last x y))

(funcion-drop-last-1 [1 2 3 5 8 13 21 34])
(funcion-drop-last-2 2 [1 2 3 5 8 13 21 34])
(funcion-drop-last-3 3 [1 2 3 5 8 13 21 34])



(defn funcion-dropWhile-1[x y] (drop-while x y))
(defn funcion-dropWhile-2[x y] (drop-while x y))
(defn funcion-dropWhile-3[x y] (drop-while x y))
(funcion-dropWhile-1 #(> 1 %) [1 2 3 5 8 13 21 34])
(funcion-dropWhile-2 #(> 2 %) [1 2 3 5 8 13 21 34])
(funcion-dropWhile-3 #(> 3 %) '(1 2 3 5 8 13 21 34))


(defn funcion-every?-1 [x] (every? even? x))
(defn funcion-every?-2[x] (every? even? x))
(defn funcion-every?-3[x] (every? even? x))
(funcion-every?-1 '(1 2 3 5 8 13 21 34))
(funcion-every?-2 [1 2 3 5 8 13 21 34])
(funcion-every?-3 [2 4 6 8 10 12 14 16])



(defn funcion-filterv-1 [x y] (filterv x y))
(defn funcion-filterv-2 [x y] (filterv x y))
(defn funcion-filterv-3 [x y] (filterv x y))



(funcion-filterv-1 even? (range 50))
(funcion-filterv-2 even? '(1 2 3 5 8 13 21 34))
(funcion-filterv-3 even? #{1 2 3 5 8 13 21 34})



(defn funcion-groupBy-1[x y] (group-by x y))
(defn funcion-groupBy-2 [x y] (group-by x y))
(defn funcion-groupBy-3 [x y] (group-by x y))
(funcion-groupBy-1 even? (range 20))
(funcion-groupBy-2 even? '(1 2 3 5 8 13 21 34))
(funcion-groupBy-3  :id-name [{:id 1 :name "Yui"}{:id 2 :name "Kotegawa"}{:id 3 :name "Haruna"}])



(defn función-iterate-1[x y](iterate x y))
(defn función-iterate-2 [x y] (iterate x y))
(defn función-iterate-3 [x y] (iterate x y))

(función-iterate-1 inc 2)
(función-iterate-2 inc 3)
(función-iterate-3 inc 4)


(defn función-keep-1[x y](keep x y))
(defn función-keep-2 [x y] (keep x y))
(defn función-keep-3 [x y] (keep x y))

(función-keep-1 #{1 2 3 5 8 13 21 34} (range 2 10))
(función-keep-2 even? '(1 2 3 5 8 13 21 34))
(función-keep-3 even? #{1 2 3 5 8 13 21 34})


(defn función-keep-indexed-1[x y](keep-indexed x y))
(defn función-keep-indexed-2[x y] (keep-indexed x y))
(defn función-keep-indexed-3 [x y] (keep-indexed x y))

(función-keep-indexed-1 vector [:a :b :c :d])
(función-keep-indexed-2 list [1 2 3 5 8 13 21 34])
(función-keep-indexed-3 hash-map [1 2 3 5 8 13 21 34])


(defn función-map-indexed-1[x y](map-indexed x y))
(defn función-map-indexed-2[x y] (map-indexed x y))
(defn función-map-indexed-3 [x y] (map-indexed x y))

(función-map-indexed-1  hash-set [1 2 3 5 8 13 21 34])
(función-map-indexed-2  list [:a :b :c])
(función-map-indexed-3  vector "Steel Angel Kurumi")



(defn función-mapcat-1[x y] (mapcat x y))
(defn función-mapcat-2 [x y] (mapcat x y))
(defn función-mapcat-3 [x y] (mapcat x y))

(función-mapcat-1  reverse [[0 1 2 3 4 5] [10 9 8 7 6] [11 12 13 14 15]])
(función-mapcat-2  reverse [[1 2] [2 2] [2 3]])
(función-mapcat-3  reverse [1 2 3 4])




(defn funcion-mapv-1[x y z] (mapv x y z))
(defn funcion-mapv-2[x y z] (mapv x y z))
(defn funcion-mapv-3[x y z] (mapv x y z))

(funcion-mapv-1 + [0 1 2 3 4 5] [4 5 6])
(funcion-mapv-2 + [0 1 2 3 4 5] [4 5 6])
(funcion-mapv-3 - [0 1 2 3 4 5] [4 5 6])



(defn funcion-merge-with-1 [x y z] (merge-with x y z))
(defn funcion-merge-with-2[x y z] (merge-with x y z))
(defn funcion-merge-with-3[x y z] (merge-with x y z))

(funcion-merge-with-1 + {:a 1 :b 3 :c 4} {:a 5 :b 6 :c 7})
(funcion-merge-with-2 + {:a 1 :b 3 :c 4} {:a -1 :b -2 :c -3})
(funcion-merge-with-3 + {"1" ["a" "b" "c"] "2" ["d" "e" "f"]}
                      {"1" ["1" "2" "3"] "2" ["4" "5" "6"]})




(defn funcion-no-any?-1[x y] (not-any? x y))
(defn funcion-no-any?-2 [x y] (not-any? x y))
(defn funcion-no-any?-3 [x y] (not-any? x y))

(funcion-no-any?-1 odd? '(1 2 3 4 5 6))
(funcion-no-any?-2 odd? [1 2 3 4 5 6])
(funcion-no-any?-3 odd? [1 2 3 4 5 6])




(defn funcion-not-every?-1[x y] (not-every? x y))
(defn funcion-not-every?-2[x y] (not-every? x y))
(defn funcion-not-every?-3[x y] (not-every? x y))

(funcion-not-every?-1  even? (range 20))
(funcion-not-every?-2  odd? (range 30))
(funcion-not-every?-3  odd? (range 40))



(defn funcion-parttition-by-1 [x y](partition-by y x))
(defn funcion-parttition-by-2[x y](partition-by y x))
(defn funcion-parttition-by-3[x y](partition-by y x))

(funcion-parttition-by-1 [1 2 3 4 5 ] #(= 2 %))
(funcion-parttition-by-2 [1 2 3 4 5] #(= 3 %))
(funcion-parttition-by-3 [1 2 3 4 5] #(= 4 %))



(defn función-reduce-kv-1[x y z](reduce-kv x y z))
(defn función-reduce-kv-2 [x y z] (reduce-kv x y z))
(defn función-reduce-kv-3 [x y z] (reduce-kv x y z))
(función-reduce-kv-1 assoc {} {:a 1 :b 2 :c 3})
(función-reduce-kv-2 assoc {} {:a 1 :b 2 :c 3 :d 4})
(función-reduce-kv-3 vector [{:a 1 :b 2}] [{:a 3 :b 4}])


(defn funcion-remove-1[x y] (remove x y))
(defn funcion-remove-2[x y] (remove x y))
(defn funcion-remove-3[x y] (remove x y))
(funcion-remove-1 pos? '(-2 0 1 2))
(funcion-remove-2 pos? '(-3 0 1 2))
(funcion-remove-3 pos? '(1 2 3 4 5))


(defn funcion-reverse-1[x] (reverse x))
(defn funcion-reverse-2[x] (reverse x))
(defn funcion-reverse-3[x] (reverse x))

(funcion-reverse-1 [1 2 3 4 ])
(funcion-reverse-2 '(1 2 3 4 ))
(funcion-reverse-3 ["Michiru" "Megumi" "Katou"])

(defn funcion-some-1[x y] (some x y))
(defn funcion-some-2[x y] (some x y))
(defn funcion-some-3[x y] (some x y))

(funcion-some-1 even? '(1 2 3 4))
(funcion-some-2 even? [1 2 3 4])
(funcion-some-3 even? (range 10))



(defn funcion-short-by-1[x y] (sort-by x y))
(defn funcion-short-by-2 [x y] (sort-by x y))
(defn funcion-short-by-3  [x y] (sort-by x y))

(funcion-short-by-1 count ["Moka" "Mizore" "Ruby"])
(funcion-short-by-3 count #{"Moka" "Mizore" "Ruby"})
(funcion-short-by-2 count '("Moka" "Mizore" "Ruby"))




(defn funcion-split-with-1[x y] (split-with x y))
(defn funcion-split-with-2 [x y] (split-with x y))
(defn funcion-split-with-3[x y] (split-with x y))
(funcion-split-with-1 (partial >= 2) (range 10))
(funcion-split-with-2 (partial >= 3) (range 10))
(funcion-split-with-3 (partial >= 4) (range 10))



(defn funcion-take-1[x y] (take x y))
(defn funcion-take-2[x y] (take x y))
(defn funcion-take-3[x y] (take x y))

(funcion-take-1 2 '(1 2 3 4 5))
(funcion-take-2 3 [1 2 3 4 5 ])
(funcion-take-3 4 [1 2 3 4 5])





(defn funcion-take-last-1  [x y] (take-last x y))
(defn funcion-take-last-2[x y] (take-last x y))
(defn funcion-take-last-3[x y] (take-last x y))

(funcion-take-last-1 3 (range 10))
(funcion-take-last-2 5 (range 10))
(funcion-take-last-3 8 (range 10))



(defn funcion-take-nth-1 [x y] (take-nth x y))
(defn funcion-take-nth-2[x y] (take-nth x y))
(defn funcion-take-nth-3[x y] (take-nth x y))

(funcion-take-nth-1 3 '(1 2 3 4 5 6))
(funcion-take-nth-2 4 [1 2 3 4 5 6])
(funcion-take-nth-3 5 {:a 1 :b 2 :c 3 :d 4 :e 5 :f 6})



(defn funcion-take-while-1[x y] (take-while x y))
(defn funcion-take-while-2 [x y] (take-while x y))
(defn funcion-take-while-3 [x y] (take-while x y))

(funcion-take-while-1 #(> 2 %)[-2 -1 0 1 2 3])
(funcion-take-while-2 #(> 3 %) '( -2 -1 0 1 2 3))
(funcion-take-while-3 #(> 4 %) [-2 -1 0 1 2 3 4])





(defn función-update-1[x y z](update x y z))
(defn función-update-2 [x y z] (update x y z))
(defn función-update-3 [x y z] (update x y z))
(función-update-1 [1 2 3] 0 inc)
(función-update-2 [1 2 3] 1 inc)
(función-update-3 [1 2 3] 0 dec)


(defn función-update-in-1[x y z] (update-in x y z))
(defn función-update-in-2 [x y z] (update-in x y z ))
(defn función-update-in-3 [x y z](update-in x y z))

(función-update-in-1 {1 {:b 3}} [1 :b] inc)
(función-update-in-2 {:a {:b 5}} [:a :b] inc)
(función-update-in-3 [1 {:a 1 :b 2 :c 3}] [1 :c] (fnil dec 1))
